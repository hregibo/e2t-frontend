import {createStore} from 'vuex';

export const store = createStore({
    actions: {
        async getUserInformations () {
            try {
                const user = await fetch('/api/me', {
                    headers: {
                        'Authorization': `Bearer ${this.state.jwt}`,
                    }
                });
                this.state.user = await user.json();
            } catch (err) {
                // The user fetching failed
                console.error('Failed to fetch user informations', err);
            }
        },
    },
    state: {
        jwt: null,
        user: null,
    },
    mutations: {},
});
export default store;