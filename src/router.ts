import { createRouter, createWebHistory } from 'vue-router';
import Home from './components/Home.vue'
import Commands from './components/Commands.vue'
import Informations from './components/Informations.vue'
import Error404 from './components/Error404.vue'

export const router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home,
        }, {
            path: '/commands',
            name: 'commands',
            component: Commands,
        }, {
            path: '/infos',
            name: 'infos',
            component: Informations,
        }, {
            path: '/:any(.*)',
            name: 'error-404',
            component: Error404,
        }
    ],
});

export default router;
