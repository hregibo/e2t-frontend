import { createApp } from 'vue'
import App from './App.vue'
import Router from './router';
import { store } from './store';

import './index.css'


createApp(App).use(store).use(Router).mount('#app')