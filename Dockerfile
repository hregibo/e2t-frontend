FROM node:lts-slim as base

WORKDIR /app
COPY package.json package-lock.json ./
RUN npm i

COPY . .
RUN npm run build

FROM nginx:1.21.4 as production

WORKDIR /usr/share/nginx/
COPY --from=base /app/dist/ ./html
COPY --from=base /app/nginx.conf /etc/nginx/conf.d/default.conf
